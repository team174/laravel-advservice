<?php

Route::get('/', 'IndexController@index');

Route::get('/how', 'IndexController@how');
Route::get('/capabilities', 'IndexController@capabilities');
Route::get('/support', 'IndexController@support');
Route::get('/about', 'IndexController@about');
Route::get('/partners', 'IndexController@partners');
Route::get('/not-realised', 'IndexController@notRealised');

Route::get('/map', 'IndexController@map');
Route::get('/search', 'SearchController@search');

Route::prefix('/object/{id}/')->group(function () {
    Route::get('/', 'ObjectController@about');
    Route::get('free', 'ObjectController@objectOrderTable');
});

