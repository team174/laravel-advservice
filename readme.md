Stack:
- php7.1
- mysql 5.7
- ruby
- gem install scss_lint

Phpstorm: scsslint, eslint

Automatic phpDoc generation for Laravel Facades (added in composer.json post-update-cmd):

Before:
1. php artisan clear-compiled  
Automatic generation:
2. php artisan ide-helper:generate  
After:
3. php artisan optimize

For generate phpDoc for models:  
php artisan ide-helper:models // need something in database

Php-linting tests, routes, app.  
Command: composer phpcs

Before push run:  
gulp production  
composer phpcs  
And after success push!

If you are in develop, phpstorm, then run:  
composer generate

On production server:  
npm install --production
composer install --no-dev
gulp --production