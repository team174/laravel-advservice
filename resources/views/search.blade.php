@extends('layouts.main')

@section('content')
    <div class="container container-search">
        @include('components.search.search-box')
        @include('components.search.fast-info')
        <div class="row content">
            <div class="col-md-4">
                @include('components.search.filters')
            </div>
            <div class="col-md-8">
                @include('components.search.content')
            </div>
        </div>
        <div class="text-center">
            @include('components.pagination')
        </div>
    </div>
@endsection