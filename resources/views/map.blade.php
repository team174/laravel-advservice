@extends('layouts.main')

@section('content')
<div class="spinner"></div>
<div class="map" id="map">
</div>
{{-- mode=debug удалить на production
--}}
@push('script-head')
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&coordorder=longlat&mode=debug" defer></script>
@endpush
{{-- longlat — [долгота, широта]. Но на сервере, как я полагаю, долгота и ширина. В общем какой-то сюрприз! --}}
@endsection