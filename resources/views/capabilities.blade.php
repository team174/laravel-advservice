@extends('layouts.main')

@section('content')
    <div class="container container-capabilities">
        <div class="row">
            <div class="col-md-4 capability">
                <img src="//placehold.it/500x500" alt="" class="img-responsive">
                <div class="descr">
                    <h3>title</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, ex in iure magni provident quidem. Accusamus amet delectus ex inventore ipsam magnam, nemo provident qui reprehenderit suscipit. Maiores, nisi, rem?</p>
                </div>
            </div>

            <div class="col-md-4 capability">
                <img src="//placehold.it/500x500" alt="" class="img-responsive">
                <div class="descr">
                    <h3>title</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, ex in iure magni provident quidem. Accusamus amet delectus ex inventore ipsam magnam, nemo provident qui reprehenderit suscipit. Maiores, nisi, rem?</p>
                </div>
            </div>

            <div class="col-md-4 capability">
                <img src="//placehold.it/500x500" alt="" class="img-responsive">
                <div class="descr">
                    <h3>title</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, ex in iure magni provident quidem. Accusamus amet delectus ex inventore ipsam magnam, nemo provident qui reprehenderit suscipit. Maiores, nisi, rem?</p>
                </div>
            </div>
        </div>
    </div>
@endsection