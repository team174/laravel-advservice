@extends('layouts.main')

@section('content')
    <div class="container container-object-info">
        <div class="row">
            <div class="col-md-6">
                <div class="row main-descr">
                    <div class="col-xs-6">
                        <img src="//placehold.it/500x500" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-6">
                        <p class="descr">г. Москва, проспект Ленина, 94</p>
                        <p class="descr"><b>Тип рекламоносителя:</b> Билборд</p>
                        <p class="descr"><b>Освещение:</b> {{ true ? 'Да' : 'Нет' }}</p>
                        <p class="descr"><b>GRP:</b> 3,6</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="order-status-wrapper">
                    @for($i =0; $i < 6; $i++)
                        <div class="order-status-item {{ $i < 5 ? ($i % 2 ? 'captured' : 'free') : 'hz' }}">
                            Июль<br>
                            {{ $i < 5 ? ($i % 2 ? 'Занят' : '23 500') : 'Уточнить' }}
                        </div>
                    @endfor
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row navigation">
                    <div class="col-xs-6">
                        <a href="#" class="btn btn-block btn-primary">Посмотреть на карте</a>
                        <a href="#" class="btn btn-block btn-primary">Посмотреть на Яндекс панорвме</a>
                    </div>
                    <div class="col-xs-6">
                        <div class="text-center">
                            <a href="#" class="btn btn-lg btn-default">A</a>
                            <a href="#" class="btn btn-lg btn-default">B</a>
                            <a href="#" class="btn btn-lg btn-default">C</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 text-center">
                <h2>Итого 47 000 руб.</h2>
                <a href="#" class="btn btn-lg btn-success">Добавить в корзину</a>
                <h3>Возникли вопросы?<br>Позвоните нас по телефону<br>8 800 00 00<br>(звонок по России бесплатный)</h3>
            </div>
        </div>

        <div class="row">
            <h3 class="text-muted">Объекты поблизости</h3>
            <div class="row">
                @for($i = 0; $i < 2; $i++)
                    @include('components.suggest-neat-object')
                @endfor
            </div>
        </div>
    </div>
@endsection