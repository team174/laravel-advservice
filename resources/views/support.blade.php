@extends('layouts.main')

@section('content')
    <div class="container container-support">
        <h1>Поддержка</h1>
        <div class="row">
            <div class="col-md-6">
                <h2>one</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis, eaque esse est et
                    excepturi fuga hic incidunt iure iusto magnam magni, omnis perferendis, possimus praesentium rem
                    reprehenderit sint voluptates?
                </p>
            </div>
            <div class="col-md-6">
                <h2>two</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis, eaque esse est et
                    excepturi fuga hic incidunt iure iusto magnam magni, omnis perferendis, possimus praesentium rem
                    reprehenderit sint voluptates?
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2>one</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis, eaque esse est et
                    excepturi fuga hic incidunt iure iusto magnam magni, omnis perferendis, possimus praesentium rem
                    reprehenderit sint voluptates?
                </p>
            </div>
            <div class="col-md-6">
                <h2>two</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis, eaque esse est et
                    excepturi fuga hic incidunt iure iusto magnam magni, omnis perferendis, possimus praesentium rem
                    reprehenderit sint voluptates?
                </p>
            </div>
        </div>
    </div>
@endsection