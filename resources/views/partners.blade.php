@extends('layouts.main')

@section('content')
    <div class="container-partners">
        <div class="wrapper">
            <div class="container">
                <h1>Наши партнёры</h1>
                <div class="gap"></div>
                <div class="gallery">
                    <img src="//placehold.it/200x200" alt="" class="img-responsive">
                    <img src="//placehold.it/200x200" alt="" class="img-responsive">
                    <img src="//placehold.it/200x200" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
@endsection