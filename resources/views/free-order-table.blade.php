@extends('layouts.main')

@section('content')
    <div class="container container-order-table">
        @include('components.search.search-box')
        @include('components.free-order-table')
        <div class="text-center">
            @include('components.pagination')
        </div>
    </div>
@endsection