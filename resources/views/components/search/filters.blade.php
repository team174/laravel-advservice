<div class="filters">
    <div class="section center">
        <a href="{{ action('IndexController@map') }}" class="btn btn-lg btn-primary">Показать карту</a>
    </div>
    <div class="section">
        <h3>Цена</h3>
        <div class="row prices-range">
            <div class="col-xs-6">
                <span>33 550</span>
            </div>
            <div class="col-xs-6">
                <span>33 550</span>
            </div>
        </div>
        <label>
            <input type="checkbox" name="free">
            Только свободные
        </label>
    </div>
    <div class="section">
        <h3>Район</h3>
    </div>
    <div class="section">
        <h3>Сторона</h3>
    </div>
    <div class="section">
        <h3>Освещение</h3>
    </div>
    <div class="section">
        <h3>Таргетинг</h3>
    </div>
    <div class="section">
        <h3>Рейтинг</h3>
    </div>
    <div class="section">
        <label>
            <input type="checkbox" name="hot">
            Горячие предложения
        </label>
        <button class="btn btn-default">Убрать все фильтры</button>
    </div>
</div>