<div class="row content-item">
    <div class="col-xs-3 img-holder">
        <img src="//placehold.it/300x300" alt="" class="img-responsive">
    </div>
    <div class="col-xs-6 main-descr">
        <p class="descr">г. Москва, проспект Ленина, 94</p>
        <p class="descr"><b>Тип рекламоносителя:</b> Билборд</p>
        <p class="descr"><b>Освещение:</b> {{ true ? 'Да' : 'Нет' }}</p>
        <p class="descr"><b>GRP:</b> 3,6</p>
        <div class="sides-wrapper">
            <button class="side btn btn-sm btn-primary">A</button>
            <button class="side btn btn-sm btn-primary">B</button>
            <button class="side btn btn-sm btn-primary">C</button>
        </div>
        <div class="free-summary">
            <div class="free-summary-item">
                <div class="headline">Июнь</div>
                Свободен<br><b>23 500</b>
            </div>
            <div class="free-summary-item">
                <div class="headline">Июнь</div>
                Свободен<br><b>23 500</b>
            </div>
            <div class="free-summary-item">
                <div class="headline">Август</div>
                Свободен<br><b>23 500</b>
            </div>
        </div>
    </div>
    <div class="col-xs-3 actions">
        <p class="price"><b>A:</b> 23 500 руб</p>
        <a href="#" class="btn btn-lg btn-block btn-primary">Заказать</a>
        <a href="#" class="btn btn-lg btn-block btn-primary">Посмотреть</a>
    </div>
</div>