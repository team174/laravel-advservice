<table class="table table-striped table-hover">
    <thead>
        <tr>
            <td>Улица</td>
            <td>Тип конструкции</td>
            <td>Освещение</td>
            <td>Сторона</td>
            <td>GPR</td>
            <td>Монтаж</td>
            <td>Стоимость</td>
            <td>Статус</td>
        </tr>
    </thead>
    @for($i = 0; $i < 8; $i++)
        <tr>
            <td>Ленина, 94</td>
            <td>Биллборд</td>
            <td>Да</td>
            <td>А</td>
            <td>3,6</td>
            <td>1300</td>
            <td>23500</td>
            <td>{{ $i % 2 ? 'Свободен' : 'Занят' }}</td>
        </tr>
    @endfor
</table>