@extends('layouts.main')

@section('content')
    <div class="container container-index">
        <p class="lead">Это идеальное место для вашей рекламы по выгодной цене</p>
        <div class="search-box">
            <div class="search-group">
                <input type="text" class="form-control" name="city" title="" placeholder="Город">
                <input type="text" class="form-control" name="street" title="" placeholder="Улица">
                <input type="text" class="form-control" name="type" title="" placeholder="Тип рекламоносителя">
                <input type="text" class="form-control" name="duration" title="" placeholder="Период размещения">
                <input type="submit" class="form-control" value="Поиск">
            </div>
            <div class="checkbox-group">
                <label>
                    <input type="checkbox" name="free">
                    Показать только свободные
                </label>

                <label>
                    <input type="checkbox" name="hot">
                    Только горячие предложения
                </label>
            </div>
        </div>
    </div>
@endsection