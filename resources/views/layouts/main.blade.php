<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Map</title>
    <link rel="stylesheet" href="/css/main.min.css">
    {{-- С defer у нас асинхронная загрузка, позволяет загружать и рисовать DOM; а также соблюдать порядок загрузки скриптов.
     А также гарантирует, что DOM готов.
     https://learn.javascript.ru/external-script
     --}}
    @stack('script-head')
    <script src="/js/main.min.js" defer></script>
</head>
<body>

@include('components/top-nav')

@yield('content')

@include('components/footer')

</body>
</html>