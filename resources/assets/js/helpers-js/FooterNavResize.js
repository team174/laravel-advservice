import {$} from '~/helpers-js/Component.js';

$(document).ready(function() {
	resize();
	function resize() {
		$('body').css('padding-top', $('nav').height());
		$('.map').css('height', $('html').height() - $('nav').height() - $('footer').height());
	}
	$(window).resize(function() {
		resize();
	});
});
