import {Component, $} from '~/helpers-js/Component.js';

export default Component.create('spinner', class {
	constructor($block) {
		this.block = $block[0];
		this.$block = $block;
		this.init();
	}

	init() {
		$(this.block).css('margin-top', $('.map').height() / 2 - $(this.block).height());
		$(this.block).css('margin-left', $('.map').width() / 2 - $(this.block).width());
		
		this.hide();
	}

	hide() {
		var self = this;
		if($('ymaps').length || window.ymaps === undefined) {
			$(this.block).hide();
		} else {
			setTimeout(function () {
				self.hide();
			}, 100);
		}
	}
});