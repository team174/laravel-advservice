import {Component} from '~/helpers-js/Component.js';

export default Component.create('map', class {
	constructor($block) {
		this.block = $block[0];
		this.$block = $block;
		if (window.ymaps != undefined) {
			ymaps.ready(() => {
				this.init();
			});
		}
	}

	init() {
		let myMap = '';
		// определяем местоположение и ставим
		ymaps.geolocation.get({
			// Выставляем опцию для определения положения по ip
			provider: 'yandex',
			// Карта автоматически отцентрируется по положению пользователя.
			mapStateAutoApply: true
		}).then(function(result) {
			myMap = new ymaps.Map('map', {
				center: [result.geoObjects.position[0], result.geoObjects.position[1]],
				zoom: 10
			});

			//http://localhost:3001/api/v1/features/within/tiles?bbox=%t&clusterize=1&zoom=%z
			const objectManager = new ymaps.RemoteObjectManager('http://localhost:3001/api/v1/features/within/tiles?bbox=%t&clusterize=1&zoom=%z');
			myMap.geoObjects.add(objectManager);
			// отображаем только с типом Banner
			setTimeout(function() {
				objectManager.setUrlTemplate('http://localhost:3001/api/v1/features/within/tiles?bbox=%t&clusterize=1&zoom=%z&q=kind=Banner');
				objectManager.reloadData();
			}, 6000);
		});
	}
});
