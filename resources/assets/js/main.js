import $ from 'jquery';
import {Component} from './helpers-js/Component';
import './components/map';
import './components/spinner';
import './helpers-js/FooterNavResize';

global.$ = global.jQuery = $;

require('./bootstrap');

$(() => {
	Component.initAll();
});
