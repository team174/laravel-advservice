const gulp = require('gulp');

const scss = require('gulp-sass');
const moduleImporter = require('sass-module-importer');
const stripCssComments = require('gulp-strip-css-comments');

const browserify = require('browserify');
const babelify = require('babelify');

const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');

const gutil = require('gulp-util');
const production = !!gutil.env.production;

if (!production) {
	/*
	eslint no-var:0
	*/
	var chalk = require('chalk');
	var sourcemaps = require('gulp-sourcemaps');
	var browserSync	= require('browser-sync').create();
}

const clean = require('gulp-clean');

const mainJSDir = './resources/assets/js/';
const mainJS = 'main.js';
const buildJSPath = 'public/js';

const scssDir = './resources/assets/scss/**/*.scss';
const buildSCSSPath = 'public/css';

gulp.task('clean-js-folder', function() {
	gulp.src('public/js', {read: false})
		.pipe(clean());
});

gulp.task('clean-css-folder', function() {
	gulp.src('public/css', {read: false})
		.pipe(clean());
});

function bundleJS(bundler) {
	return bundler.bundle()
		.on('error', function(err){
			// print the error (can replace with gulp-util)
			console.log(err.message);
			// end this stream
			this.emit('end');
		})
		.pipe(source(mainJS))
		.pipe(buffer())
		.pipe(rename({ suffix: '.min' }))
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(buildJSPath))
		.pipe(browserSync.reload({stream: true}));
}

gulp.task('js-dev', ['clean-js-folder'], function() {
	const bundler = browserify(mainJSDir + mainJS, { debug: true }).transform(babelify);
	bundleJS(bundler);
});

gulp.task('js-prod', ['clean-js-folder'], function() {
	const bundler = browserify(mainJSDir + mainJS).transform(babelify);
	bundler.bundle()
		.pipe(source(mainJS))
		.pipe(buffer())
		.pipe(rename({ suffix: '.min' }))
		.pipe(uglify())
		.pipe(gulp.dest(buildJSPath));
});

gulp.task('browser-sync', function() {
	browserSync.init({
		proxy: 'nginx:80', // your web site
		ui: {
			port: 8081,
			weinre: {
				port: 8083
			}
		},
		port: 8000,
		notify: false,
		open: false // Stop the browser from automatically opening
	});
});

gulp.task('scss-dev', ['clean-css-folder'], function() {
	return gulp.src(scssDir)
		.pipe(sourcemaps.init())
		.pipe(scss({ outputStyle: 'expanded', importer: moduleImporter()}).on('error', scss.logError))
		.pipe(rename({suffix: '.min'}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(buildSCSSPath))
		.pipe(browserSync.reload({stream: true}));
});

gulp.task('scss-prod', ['clean-css-folder'], function() {
	return gulp.src(scssDir)
		.pipe(scss({ outputStyle: 'compressed', importer: moduleImporter() }).on('error', scss.logError))
		.pipe(rename({suffix: '.min'}))
		.pipe(stripCssComments())
		.pipe(gulp.dest(buildSCSSPath));
});

// проверка на обновление страницы после изменения js
gulp.task('dev-watch', ['scss-dev', 'js-dev', 'browser-sync'], function() {
	gulp.watch('resources/assets/scss/**/*', ['scss-dev']);
	gulp.watch('resources/assets/js/**/*.js', ['js-dev']);
	gulp.watch('resources/views/**/*.php', browserSync.reload);
});

gulp.task('dev-build', ['scss-dev', 'js-dev']);

gulp.task('production', ['scss-prod', 'js-prod']);

if (production) {
	gulp.task('default', ['production']);
} else {
	gulp.task('default', ['dev-watch']);
}
