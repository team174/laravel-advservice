<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller {

    public function index() {
        return view('index');
    }

    public function map() {
        return view('map');
    }

    public function how() {
        return Redirect::action('IndexController@notRealised');
    }

    public function capabilities() {
        return view('capabilities');
    }

    public function support() {
        return view('support');
    }

    public function about() {
        return Redirect::action('IndexController@notRealised');
    }

    public function partners() {
        return view('partners');
    }

    public function notRealised() {
        return view('not-realised');
    }
}
