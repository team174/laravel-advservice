<?php

namespace App\Http\Controllers;

class ObjectController extends Controller {
    
    public function objectOrderTable() {
        return view('free-order-table');
    }

    public function about() {
        return view('object-info');
    }
}
